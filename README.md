# Tutorial ReactJS

This tutorial is based from (reference links) :
https://www.freecodecamp.org/news/the-react-handbook-b71c27b0a795/

I just want to do it personally for each page in here so i could learn more detail about it.
I try to understand how each code work. So there are some example codes which i try to write it out to test it how it works.

> Caution : This is not suitable for learning base-code purpose (if you typical person who learn from reading code). I suggest you to follow reference links as i stated before. This is only for personal purpose learning. I want to documented my progress learning

Mostly i wrote my code in /src/tutorial/ directory. You can find it in here, there are some chapter links based on referenced link i tried to follow it up. If the tutorial just explain about concept, i try review it up in my own language in markdown document (just like this).

I used /src/App.js to try it out via import the /src/tutorial/ file. You can see there are some commented code which is from the code that i tried it out.

## About Me

Currently (when i write this README.md), i am undergraduate student computer scientist in Universitas Indonesia.
I have keen in every technology Backend, Frontend, Mobile , DevOps, Machine Learning, and even IoT.
My proudest private project so far is https://tot.bio, which combine every technology i stated before.

Now i am in my last semester before i (hope) graduated from college. I want to know the basic of ReactJS in every Aspect (I don't want only to know how to use it by example only). I hope i could finished this tutorial before my last semester begin.

Oh yeah, After i do this tutorial, i also want to documenting about my project in machine learning. I hope it doesn't only become a plan.

P.S. Sorry for bad english :P