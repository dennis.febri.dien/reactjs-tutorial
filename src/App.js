import React from 'react';
import logo from './logo.svg';
/* 

import ArrayObjects from './tutorial/Chp1_modernJS/ArrayObjects';
import Variable from './tutorial/Chp1_modernJS/Variable'; 
import FunctionJS from './tutorial/Chp1_modernJS/FunctionJS';
import TemplateLiteral from './tutorial/Chp1_modernJS/TemplateLiteral';
import ClassesJS from './tutorial/Chp1_modernJS/ClassesJS';
import PromisesJS from './tutorial/Chp1_modernJS/PromisesJS';
import AsyncAwait from './tutorial/Chp1_modernJS/AsyncAwait';
import ECMAScript from './tutorial/Chp1_modernJS/ECMAScript';
import CompositionJS from './tutorial/Chp2_reactConcept/CompositionJS';
import JSXJavaScript from './tutorial/Chp3_inDepthReact/JSXJavaScript';
import ComponentsJSReact from './tutorial/Chp3_inDepthReact/ComponentsJSReact';
import StatePropsJSReact from './tutorial/Chp3_inDepthReact/StatePropsJsReact';
import StateVSPropsJsReact from './tutorial/Chp3_inDepthReact/StateVSPropsJsReact';
import PropTypesJSReact from './tutorial/Chp3_inDepthReact/PropTypesJSReact';
import FragmenJSReact from './tutorial/Chp3_inDepthReact/FragmentJSReact';
import EventJSReact from './tutorial/Chp3_inDepthReact/EventsJSReact';
import LifecyclesEventJSReact from './tutorial/Chp3_inDepthReact/LifecyclesEventJSReact';
*/

import './App.css';
import FormJSReact from './tutorial/Chp3_inDepthReact/FormJSReact';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      
      {/* 
      
      This is for Commenting Tutorial Console.log so i could focus on one tutorial.
      To Run Tutorial, just uncomment the tag 
      
      */}
      {/* 
        <ArrayObjects />
        <Variable /> 
        <FunctionJS />
        <TemplateLiteral />
        <ClassesJS />
        <PromisesJS />
        <AsyncAwait />
        <ECMAScript />

        <CompositionJS />

        <JSXJavaScript />
        <ComponentsJSReact />
        <StatePropsJSReact />
        <StateVSPropsJsReact/>
        <PropTypesJSReact />
        <FragmenJSReact />
        <EventJSReact />
        <LifecyclesEventJSReact />
      */}
      <FormJSReact />
    </div>
  );
}
export default App;
