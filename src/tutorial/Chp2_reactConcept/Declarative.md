# Declarative

Tl;dr, They implements Declarative Programming for building UIs.

They don't touching DOM directly.
so they simplified it.

# Immutability

Tl;dr, the state variable will never be changed directly to make code cleaner and give us predictability.

# Pure Function

When function does not mutate objects and just return a new objects, it's called pure function.

```
const Button = props => {
    return <button>{props.message}</button>
}
```
```
class Button extends React.Component {
   render() {
        return <button>{this.props.message}</button>
    }
}
```

