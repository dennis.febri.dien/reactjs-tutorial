# Single Page Apps (SPA)

### Before

Every website need to request to server to get html frequently

### After

There are concept single page apps, so if you only want to render Front-end, you don't need to call Server. You only need to communicate with server if there's transaction data.

### Pros

- SPA feels much faster to user, because the website rendered by their own computer with javascript.
- Server consume less resource because they only focus serving efficient API.
- You could change SPA into Progressive Web Apps (support load caching and offline experience services)
- with SPA, we could seperate more easily working teams. Backend focus on API. Frontend focus UX and making use of built in backend

### Cons
- SPA is best used when there is no need for SEO (a.k.a Apps work behind a login)(hard to detected)
- Need pay a lot attention to possible memory leaks
- SPA rely heavily on JS (poor experience for low power devices)
- Some visitors may disable javaScript

### Overriding the navigation

You need to manage URLs manually. Some frameworks take care of them (like Embers), some don't. If you want to take care manually, you need "React Router"

There's also problem with History problem. it called "brocken back button". when navigating inside the application, the URL didn't change, so you need to override back button and reload using "History API", another good library is "React Router".