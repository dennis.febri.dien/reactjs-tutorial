# Unidirectional Data Flow

The core of the concept is "uni" directional. Data only could flow with one way.

In React this means that:
- state is passed to the view and to child components
- actions are triggered by the view
- actions can update the state
- the state change is passed to the view and to child components

The view is a result of the application state. State can only change when actions happen.
When actions happen, the state is updated.

Data can't flow to the opposite way. This is to makes sure code less error and easy to find a bugs