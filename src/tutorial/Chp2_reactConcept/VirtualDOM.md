# Virtual DOM

DOM is Document Object Model, it is tree representating of html that is going down from \<html> to their own child
which is called nodes. DOM has some API to help operating with its content.

some of them are :

>document.getElementById(id)
>document.getElementsByTagName(name)
>document.createElement(name)
>parentNode.appendChild(node)
>element.innerHTML
>element.style.left
>element.setAttribute()
>element.getAttribute()
>element.addEventListener()
>window.content
>window.onload
>window.dump()
>window.scrollTo()

Tl;dr , react manipulating DOM API to re-rendering page with their style
for example, they will automatically rendering page if there's change on state react

