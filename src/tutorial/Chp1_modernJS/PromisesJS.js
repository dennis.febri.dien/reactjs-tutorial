import React from 'react';
import * as utils from './Utils'; // TODO Don't forget to change it


const TAG_TUTORIAL = "[PROMISE]"
let tutorial = "This is example basic promises" 


function PromisesJS() { // TODO Don't forget to change it
    return (
        <div>
            This Class or function only for learning about Promises in JS
        </div>
    );
}

let done = true

const isItDoneYet = new Promise((resolve, reject) => {
    if (done) {
        const workDone = 'Here is the thing I built (isItDoneYet)'
        resolve(workDone)
    } else {
        const why = 'Still working on something else (isItDoneYet)'
        reject(why)
    }
})


const checkIfItsDone = () => {
    isItDoneYet
        .then(ok => {
            console.log(ok)
        })
        .catch(err => {
            console.error(err)
        })
}
utils.explanation(TAG_TUTORIAL, tutorial, [""]);
checkIfItsDone();

/*
    Another Example using Promise Chain
*/

const status = response => {
    if (response.status >= 200 && response.status < 300) {
        return Promise.resolve(response)
    }
        return Promise.reject(new Error(response.statusText))
    }
const json = response => response.json()


tutorial = `Here's some example promises in case of fetching json data
If you see the result promises and this utils output in not sequential
you will know that the process if the promises is running asyncronously.`
utils.explanation(TAG_TUTORIAL, tutorial, [""]);
fetch('/todos.json')
    .then(status)
    .then(json)
    .then(data => {
        console.log('Request succeeded with JSON response (fetch //todos.json)', data)
    })
    .catch(error => {
        console.log('Request failed (fetch //todos.json)', error)
    })
    

tutorial = `Here's some example to catch error in promises`
utils.explanation(TAG_TUTORIAL, tutorial, [""]);

/*
If you want too see the error, uncomment this


new Promise((resolve, reject) => {
    throw new Error('Error Throw')
}).catch(err => {
    console.error(err)
})
    // or
new Promise((resolve, reject) => {
    reject('Error Reject')
}).catch(err => {
    console.error(err)
})

//OR

new Promise((resolve, reject) => {
    throw new Error('Error1')
}).catch(err => {
        throw new Error(err)
    }).catch(err => {
            console.error(err)
        })
    
*/

tutorial = "there are other method in promises, Promise.all(), Promise.race(), etc"
utils.explanation(TAG_TUTORIAL, tutorial, [""])

const f1 = fetch('/todos.json')
const f2 = fetch('/todos2.json')

Promise.all([f1, f2])
    .then(res => {
        console.log('Array of results (Promise.all)', res)
    })
    .catch(err => {
        console.error(err)
    })

    //OR

Promise.all([f1, f2]).then(([res1, res2]) => {
    console.log('Results (Promise.all)', res1, res2)
    })
        

tutorial = "Example for promise.race()"
utils.explanation(TAG_TUTORIAL, tutorial, [""])


const promiseOne = new Promise((resolve, reject) => {
    setTimeout(resolve, 500, 'one')
})

const promiseTwo = new Promise((resolve, reject) => {
    setTimeout(resolve, 100, 'two')
})
    
Promise.race([promiseOne, promiseTwo]).then(result => {
    console.log(result, ("Promise.race")) // 'two'
})

/*
If you get the Uncaught TypeError: undefined is not a promise error in the console, make sure
you use new Promise() instead of just Promise()
*/







//utils.explanation(TAG_TUTORIAL,tutorial,"OUTPUT HERE")
//utils.newSubTutorial("Part x: TITLE HERE")

export default PromisesJS; // TODO Don't forget to change it