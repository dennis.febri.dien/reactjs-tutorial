import React from 'react';
import * as utils from './Utils'; // TODO Don't forget to change it

function AsyncAwait() { // TODO Don't forget to change it
    return (
        <div>
            This Class or function only for learning about Async and Await in JS
        </div>
    );
}
// `` for copying template literal

const TAG_TUTORIAL = "[AsyncAwait]"
let tutorial = "Asych function returns a promise, you will see that async is same as promise" 

const doSomethingAsync = () => {
    return new Promise(resolve => {
        setTimeout(() => resolve('I did something'), 3000)
    })
}    

const doSomething = async () => {
    console.log(await doSomethingAsync())
}
utils.explanation(TAG_TUTORIAL,tutorial,["\n"])
console.log("Before")
doSomething()
console.log("After")

tutorial = "You can see this example for comparison between Normal Async and Promise"

const aFunctionAsych = async () => {
    return 'test-Asych'
}
//aFunctionAsych().then(alert)

const aFunction = async () => {
    return Promise.resolve('test-Promise')
}
//aFunction().then(alert) // This will alert 'test'
// you will notice the alert caused rendering javascript halt

/*
const getFirstUserDataPromise = () => {
    return fetch('/users.json') // get users list
        .then(response => response.json()) // parse JSON
        .then(users => users[0]) // pick first user
        .then(user => fetch(`/users/${user.name}`)) // get user data
        .then(userResponse => response.json()) // parse JSON
}
getFirstUserDataPromise()

const getFirstUserData = async () => {
    const response = await fetch('/users.json') // get users list
    const users = await response.json() // parse JSON
    const user = users[0] // pick first user
    const userResponse = await fetch(`/users/${user.name}`) // get user data
    const userData = await user.json() // parse JSON
    return userData
}
getFirstUserData()
    
*/

const promiseToDoSomething = () => {
    return new Promise(resolve => {
        setTimeout(() => resolve('I did something'), 10000)
    })
}
const watchOverSomeoneDoingSomething = async () => {
    const something = await promiseToDoSomething()
    return something + ' and I watched'
}
const watchOverSomeoneWatchingSomeoneDoingSomething = async () => {
    const something = await watchOverSomeoneDoingSomething()
    return something + ' and I watched as well'
}
watchOverSomeoneWatchingSomeoneDoingSomething().then(res => {
    console.log(res)
})
    


//utils.explanation(TAG_TUTORIAL,tutorial,"OUTPUT HERE")
//utils.newSubTutorial("Part x: TITLE HERE")

export default AsyncAwait; // TODO Don't forget to change it