import React from 'react';
import * as utils from './Utils';


function TemplateLiteral() {
    return (
        <div>
            This Class or function only for learning about TemplateLiteral
        </div>
    );
}

const multipleLine =  `
Hi!
My Name is Dennis Febri Dien
I am Computer Scientist
`

const TAG_TUTORIAL = "[ArrayObjects]";
var tutorial = "using ` for multiplelines"
utils.explanation(TAG_TUTORIAL, tutorial, [multipleLine]);


const foo = (inp) => inp%2 === 0;
let input1 = 6
const output1 = ` input1 % 2 = ${foo(input1) ? 'correct' : 'incorrect'}`
tutorial = "using ` for interpolation (execute command in template)"
utils.explanation(TAG_TUTORIAL, tutorial, [output1]);


export default TemplateLiteral;