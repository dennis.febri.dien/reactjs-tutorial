import React from 'react';
import * as utils from './Utils';


function FunctionJS() {
    return (
        <div>
            This Class or function only for learning about function in JS
        </div>
    );
}

const TAG_TUTORIAL = "[FunctionJS]";
var tutorial;
var subTutorial;


tutorial = "This tutorial is to learn about function in modern JS";
console.log(tutorial);

/*
-----------------------------------------------------------------------
*/

subTutorial = "Part 1. Arrow Function"
utils.newSubTutorial(subTutorial);


tutorial = "[called introduction with arrowfunction]"
console.log(tutorial);
const introduction = () => {
    const intro = "My name is ";
    const name = "Dennis Febri Dien";
    console.log(TAG_TUTORIAL,intro, name);
}    
introduction();


tutorial = "[you can called arrow function without block]";
console.log(tutorial);
const callIntroduction = () => introduction();
callIntroduction();


tutorial = "[you can also add parameter in arrow function]";
console.log(tutorial);
const introductionParam2 = (intro, name) => {
    console.log(TAG_TUTORIAL, intro, name);
}
const callIntroductionParam2 = (intro, name) => introductionParam2(intro, name);
callIntroductionParam2("J'appelle ", "Dennise Febri Dien");


tutorial = "[Experiment : Point Free Style] callIntroductionPFS";
console.log(tutorial);
const introductionPFS = name => {
    const intro = "My name is ";
    console.log(TAG_TUTORIAL, intro, name);
}
introductionPFS("DennisA!");
const callIntroductionPFS = introductionPFS;
callIntroductionPFS("Point Free Style");


/*
-----------------------------------------------------------------------
*/


subTutorial = "Part 2. Implicit Return Function";
utils.newSubTutorial(subTutorial);


tutorial = "arrow function allow you to return value without declare it";
console.log(callIntroductionPFS("hallo"), "[called block function]" );
// console.log(callIntroductionPFS("hallo").intro, "[called attribute function assigned variable]" ); failed


const myRealName = () => ({
    firstName : 'Dennis',
    middleName : 'Febri',
    lastName : 'Dien',
});
console.log(myRealName, "[Called object/ some kind of dictionary]");


/*
-----------------------------------------------------------------------
*/


subTutorial = "Part 3. how 'this' works in arrow functions";
utils.newSubTutorial(subTutorial);

const carNormal = {
    model: 'Fiesta',
    manufacturer: 'Ford',
    fullName: function() {
        return `${this.manufacturer} ${this.model}`
    }
}

const carArrow = {
    model: 'Fiesta',
    manufacturer: 'Ford',
    fullName: () => {
        return `${this.manufacturer} ${this.model}`
    }
}

console.log(carNormal.fullName(), "[Using normal Function]");
// console.log(carArrow.fullName(), "[Using Arrow Function]"); got error

console.log("[Another example]");

/*
const link = document.querySelector('#link')
link.addEventListener('click', () => {
    // this === window
})

const link = document.querySelector('#link')
link.addEventListener('click', function() {
    // this === link
})
*/

export default FunctionJS;