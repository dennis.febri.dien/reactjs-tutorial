import React from 'react';
import * as utils from './Utils'; // TODO Don't forget to change it

function ClassesJS() { // TODO Don't forget to change it
    return (
        <div>
            This tutorial to explain Classes in Modern JS    
        </div>
    );
}

class Person {
    constructor(name) {
        this.job = "unemployed"
        this.name = name
    }

    static genericHello() {
        return 'Hello'
    }

    get name() {
        return this._name;
    }

    set name(val) {
        this._name = val;
    }

    get job() {
        return this._job;
    }

    set job(val) {
        this._job = val;
    }

    hello() {
        return `${Person.genericHello()}, I am ${this.name}`
    }
}

const TAG_NAME = "[CLASS_JS_MODERN]"
let tutorial = "Here's some example of class in JS Modern" 

const dennis = new Person('Dennis')
utils.explanation(TAG_NAME, tutorial, [dennis.hello()])

class Programmer extends Person {
    constructor(name) {
        super(name);
        super.job = "Programmer";
    }

    hello() {
        return `${super.hello()} I am a ${super.job}.`
    }
}

tutorial = `Here's some example of class heritance in JS Modern
we have super.setJob to assign attribute parent this.job. If there are no
set setJob, then, you can't change the value`
const palol = new Programmer('Palolino')
utils.explanation(TAG_NAME, tutorial, [palol.hello()])


//utils.explanation("ADD TAG IN HERE","EXPLAINATION HERE","OUTPUT HERE")
//utils.newSubTutorial("Part x: TITLE HERE")

export default ClassesJS; // TODO Don't forget to change it