import React from 'react';
import * as utils from './Utils';


function ArrayObjects() {
    return (
        <div>
            This Class or function only for learning about Array and Objects in modern JS
        </div>
    );
}

const TAG_TUTORIAL = "[ArrayObjects]";
var tutorial;
tutorial = "This tutorial is to learn about function in modern JS";
console.log(TAG_TUTORIAL, tutorial);
utils.newSubTutorial("Part 1. Basic Array JS");


const a = [1,2,3]
const b = [...a, 4,5,6]
const c = [...a];
tutorial = "const a can be concantenate using '...a', and also it could copy";
console.log(TAG_TUTORIAL, tutorial,
    a, b, c);


const hey = 'hey'
const arrayizedHey = [...hey];
tutorial = "you could also change string to array char using '...hey'\n";
console.log(TAG_TUTORIAL, tutorial,
    'before : ',hey,
    '\nAfter : ', arrayizedHey);


const f = (first, middle, last) => 
    "My name is "+first+" "+middle+" "+last
const name = ["Dennis", "Febri"];
tutorial = "use '...name as argument for function\n";
console.log(TAG_TUTORIAL, tutorial, f(...name));


const numbers = [1,2,3,4,5]
let first, second, others;
[first, second, ...others] = numbers;
tutorial = "Using rest Element on the last";
console.log(TAG_TUTORIAL, tutorial, first, second, others);
const summming = (a, b, c, d, e) => a + b + c + d + e
const resSumming = summming(...numbers)
tutorial = "using summing";
console.log(TAG_TUTORIAL, tutorial, numbers, resSumming);
utils.explanation(TAG_TUTORIAL, tutorial, numbers);


const { first1, second1, ...others1 } = {
    first1: 1,
    second1: 2,
    third1: 3,
    fourth1: 4,
    fifth1: 5
    }
const out = {first1, second1, ...others1};
console.log(out);


utils.newSubTutorial("Part 2. Arrays and Objects Deconstructing");

const person = {
    firstName: 'Tom',
    lastName: 'Cruise',
    actor: true,
    age: 54 //made up
    }

tutorial = "";
const { firstName: firstNamed , age  } = person //name: Tom, age: 54
utils.explanation(TAG_TUTORIAL, tutorial, ["you can assign some const from attribute of another const\n",
"if the name of attribute and the const is same, you can declare it with const {age} = person \n",
"if the name of attribute is different with the const we want to assign, you need declare it like this\n",
"const {firstName : firstNamed} = person. it will assign value person firstName to firstNamed"]);
console.log(firstNamed, age, person.firstName, person.age);


console.log("Another Example")
const [a1, b1] = numbers
console.log(a1, b1)

const [a2, b2, , , e2] = numbers
console.log(a2, b2, e2);


export default ArrayObjects;