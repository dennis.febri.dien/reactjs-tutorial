import React from 'react';

function Variable() {
    return (
        <div>
            This Class or function only for learning about variable in JS
        </div>
    );
}

const TAG_TUTORIAL = "[Variable] ";

var tutorial;

tutorial = " [using var to assign]";
var varEx1 = 1, varEx2 = 2;
console.log(TAG_TUTORIAL,"varEx1 : ", varEx1,"varEx2 : ", varEx2, tutorial);

let color = "red";
var colorA = "red";
{
  
  tutorial = " [using let to assign, only block used]";
  let color = "blue";
  //var colorA = "blue"; // caused error cuz "var" assign on global variable
console.log(TAG_TUTORIAL, "let the color inside the block be :",color, tutorial);
}

tutorial = " [using let to assign, called outside block, so used let color outside block]";
console.log(TAG_TUTORIAL, "let the color outside the block be :",color, tutorial);

tutorial = " [var in block can be replaced from outside block, so it's dangerous to use var]";
console.log(TAG_TUTORIAL, "color var :",colorA);


const constA = "Dennis Febri Dien"

{
  const constA = "Palol"
  tutorial = " [same as let, const is using block to prevent assign variable to global]";
  console.log(TAG_TUTORIAL, "My name is ", constA, tutorial);
}
console.log(TAG_TUTORIAL, "my name is ", constA);


export default Variable;