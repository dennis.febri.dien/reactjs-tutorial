import React from 'react';
import * as utils from './Utils'; // TODO Don't forget to change it
import defaultNameofModule from './module_custom/uppercase'
import { a, b as two, c as three} from './module_custom/another'

utils.newSubTutorial("Part 1. ECMAScript")

function ECMAScript() { // TODO Don't forget to change it
    return (
        <div>
            This Class or function only for learning about Async and Await in JS
        </div>
    );
}
// `` for copying template literal

const TAG_TUTORIAL = "[ECMA_Script]"
let tutorial = "TL;DR this is example how to import something" 

/*
Modern Way :
import package from 'module-name'

CommonJS
const package = require('module-name')
*/
utils.explanation(TAG_TUTORIAL, tutorial, [""])
console.log(defaultNameofModule("lala"))

tutorial = "This is example import using {a, b......}\n"
utils.explanation(TAG_TUTORIAL, tutorial, [a, two, three])


utils.newSubTutorial("Another Part : CORS")
tutorial = `
    Modules are fetched using cors. if you want to reference script
    from other domains, they must have a valid CORS header
    You need declare it in outside
`

utils.explanation(TAG_TUTORIAL, tutorial, [""])



export default ECMAScript; // TODO Don't forget to change it