import React from 'react';

var primaryColor = {
    color: 'black',
}

var secondaryColor = {
    color: 'blue',
}

function StateVSPropsJsReact() { // TODO Don't forget to change it
    return (
        <div>
            <PrimaryComponent />    
        </div>
    );
}
// `` for copying template literal

class PrimaryComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataPrimary: 1,
            dataSecondary: 2,
            addClickState: this.addClickState,
        }
    }

    addClickState = () => {
        this.setState({
            dataPrimary: (this.state.dataPrimary + 1),
            dataSecondary: (this.state.dataSecondary + 1),
        })
    }

    render() {
        return (
            <div style={primaryColor}>
                
                <p>These buttons is from perspective PrimaryComponent</p>
                <p>{this.state.dataPrimary}</p>
                <p>{this.state.dataSecondary}</p>
                <SecondaryComponent {...this.state}/>
            </div>
        )
    }
}

class SecondaryComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = this.props // How to transfer data
    }

    addClickState = () => {
        this.setState({
            dataPrimary: (this.state.dataPrimary + 1),
            dataSecondary: (this.state.dataSecondary + 1),
        })
    }

    addClickProps = () => this.props.addClickState();


    render() {
        return (
            <div style={secondaryColor}>
                <p>{this.state.dataPrimary}</p>
                <p>{this.state.dataSecondary}</p>
                <p>These buttons is from perspective SecondaryComponent</p>
                <p>If you call Add Props in here, SecondaryComponent will be added, but PrimaryComponent doesn't</p>
                <p>This behaviour happens because i assign state secondary state with primary state from props</p>
                <button onClick={this.addClickState}> add State from this Component (secondary) </button>
                <button onClick={this.addClickProps}> add Props from PrimaryComponent</button> 
                <TertiaryComponent values={this.state} funcButton={this.addClickState}/>
            </div>
        )
    }
}

class TertiaryComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = this.props // How to transfer data
    }

    render() {
        return (
            <div style={secondaryColor}>
                <p>{this.props.values.dataPrimary}</p>
                <p>{this.props.values.dataSecondary}</p>
                <p>These buttons is from perspective TertiaryComponent</p>
                <p>This is another example, if we wants to change value parent and child at the same time with inherited</p>
                <p>value on props</p>
                <button onClick={this.props.funcButton}> add Props from SecondaryComponent</button> 
            </div>
        )
    }
}

const TAG_TUTORIAL = "[State vs Props]"
let tutorial = "add explanation in here and replace if there's another explanation" 


export default StateVSPropsJsReact; // TODO Don't forget to change it