import React from 'react';
import PropTypes from 'prop-types';

var primaryColor = {
    color: 'black',
}

var secondaryColor = {
    color: 'blue',
}

function FragmenJSReact() { // TODO Don't forget to change it
    return (
        <div>
            <h1>This tutorial, using template StateVsPropJsReact.js (I just want to test some Proptypes before learn about fragment)</h1>
            <PrimaryComponent />    
        </div>
    );
}
// `` for copying template literal

class PrimaryComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataPrimary: 1,
            dataSecondary: 2,
            addClickState: this.addClickState,
        }
    }

    addClickState = () => {
        this.setState({
            dataPrimary: (this.state.dataPrimary + 1),
            dataSecondary: (this.state.dataSecondary + 1),
        })
    }

    render() {
        return (
            <div style={primaryColor}>
                
                <p>These buttons is from perspective PrimaryComponent, there are no proptypes right now</p>
                <p>{this.state.dataPrimary}</p>
                <p>{this.state.dataSecondary}</p>
                <SecondaryComponent />
            </div>
        )
    }
}

class SecondaryComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = this.props // How to transfer data
    }

    addClickState = () => {
        this.setState({
            dataPrimary: (this.state.dataPrimary + 1),
            dataSecondary: (this.state.dataSecondary + 1),
        })
    }

    addClickProps = () => this.props.addClickState();


    render() {
        return (
            <>
                <p>{this.state.dataPrimary}</p>
                <p>{this.state.dataSecondary}</p>
                <p>These buttons is from perspective SecondaryComponent, there are some proptypes</p>
                <p>If you call Add Props in here, SecondaryComponent will be added, but PrimaryComponent doesn't</p>
                <p>This behaviour happens because i assign state secondary state with primary state from props</p>
                <button onClick={this.addClickState}> add State from this Component (secondary) </button>
                <button onClick={this.addClickProps}> add Props from PrimaryComponent</button> 
                <TertiaryComponent values={this.state} funcButton={this.addClickState}/>
            </>
        )
    }
}


SecondaryComponent.propTypes = {
    dataPrimary: PropTypes.number,
    dataSecondary: PropTypes.number
}

SecondaryComponent.defaultProps = {
    dataPrimary: 0,
    dataSecondary: 0
}

class TertiaryComponent extends React.Component {
    constructor(props) {
        super(props)
        this.state = this.props // How to transfer data
    }

    render() {
        return (
            <div style={secondaryColor}>
                <p>{this.props.values.dataPrimary}</p>
                <p>{this.props.values.dataSecondary}</p>
                <p>These buttons is from perspective TertiaryComponent</p>
                <p>This is another example, if we wants to change value parent and child at the same time with inherited</p>
                <p>value on props</p>
                <button onClick={this.props.funcButton}> add Props from SecondaryComponent</button> 
            </div>
        )
    }
}


export default FragmenJSReact; // TODO Don't forget to change it