import React, {Component} from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

function LifecyclesEventJSReact() { // TODO Don't forget to change it
    return (
        <div>
            TODO : Write the code in this file. and this div is used to describe what do you do (just for tutorial)
            <SandboxComponentPrimary />
        </div>
    );
}

class SandboxComponentPrimary extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            level : 'Easy',
        }
    }

    changeLevelButton = newLevel => event => {
        this.setState(
            {
                level : newLevel
            }
        )
    }

    render() {
        return(
            <>
                <p>Current level :</p>
                <p> {this.state.level} </p>
                
                <button onClick={this.changeLevelButton('Easy')}>Easy</button>
                <button onClick={this.changeLevelButton('Medium')}>Medium</button>
                <button onClick={this.changeLevelButton('Hard')}>Hard</button>

                <SandboxComponentSecondary level={this.state.level} />
                
            </>
        );
    }
}

const API_CALL = 'http://dummy.restapiexample.com/api/v1/employees';


class SandboxComponentSecondary extends Component {

    
    
    constructor(props) {
        super(props)
        this.state = { 
            level : '',
            terrain : '',
            status: '',
            hits: '',
        }
    }
    
    static getDerivedStateFromProps(props, state) {
        // Return null to indicate no change to state.
        return {
            level : props.level,
            terrain : props.level === 'Easy' ? 'Grass' :
                (props.level === 'Medium' ? 'Beach' : 'Northern')
        }
    }

    tableEmployement() {
        let arr = [];
        Object.keys(this.state.hits.data[0]).forEach(function(key) {
            arr.push(key);
        });
        
        console.log(arr)
        return(    
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                    <TableCell component="th" scope="row">
                      {arr[0]}
                    </TableCell>
                    <TableCell align="right">{arr[1]}</TableCell>
                    <TableCell align="right">{arr[2]}</TableCell>
                    <TableCell align="right">{arr[3]}</TableCell>


                </TableRow>
              </TableHead>
              <TableBody>
                {this.state.hits.data.map(row => (
                  <TableRow key={row.id}>
                    <TableCell component="th" scope="row">
                      {row.id}
                    </TableCell>
                    <TableCell align="right">{row.employee_name}</TableCell>
                    <TableCell align="right">{row.employee_salary}</TableCell>
                    <TableCell align="right">{row.employee_age}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        )
    }

    apiCalled() {
        let output = ''
        if(this.state.status === '') {
            output = <p>Now Loading</p>
        } else if(this.state.status === 200) {
            output = this.tableEmployement()
        } else {
            output = <p>Error code {this.state.status}</p>
        }
        return output
        
    }

    render() {
        return(
            <>
                <p>Current level :</p>
                <p> {this.state.level} </p>
                
                <p>Current terrain :</p>
                <p> {this.state.terrain} </p>

                <p>API Called by componentDidMount</p>
                <div>
                    {
                        this.apiCalled()
                    }    
                </div>

            </>
        );
    }

    componentDidMount() {
        console.log("componentDidMount")
        fetch(API_CALL)
            .then(response =>{
                if(response.status === 200){
                    response.json().then(
                        data => this.setState({
                            hits: data,
                            status: response.status
                        })
                    )
                } else {
                    this.setState({
                        hits: null,
                        status: response.status
                    })
                }
            })
    }
}
// `` for copying template literal



export default LifecyclesEventJSReact; // TODO Don't forget to change it