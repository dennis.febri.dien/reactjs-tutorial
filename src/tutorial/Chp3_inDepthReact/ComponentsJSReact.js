import React, { Component } from 'react';
import ReactDOM from 'react-dom';

function ComponentsJSReact() { // TODO Don't forget to change it
    return (
        <div>
            This is for ComponentsJSReact Tutorial
            <ComponentsJSReact1 />
            <ComponentsJSReact2 />
        </div>
    );
}
// `` for copying template literal

const TAG_TUTORIAL = "[Components]"
let tutorial = "add explanation in here and replace if there's another explanation" 

ReactDOM.render(<h1>Hello World!</h1>, document.getElementById('sandbox'))


// Another way to define component in React

const ComponentsJSReact1 = () => {
    return (
        <div>
            <h1> Component React using const </h1>
            <p>another example below </p>
        </div>
    )
}

class ComponentsJSReact2 extends Component {
    render() {
        return (
        <div>
            <h1> Component React using class extends Component</h1>
            <p>there are another example </p>
        </div>
        )
    }
}



export default ComponentsJSReact; // TODO Don't forget to change it