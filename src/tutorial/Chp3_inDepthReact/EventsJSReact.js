import React, {Component, } from 'react';


function EventJSReact() { // TODO Don't forget to change it
    return (
        <div>
            TODO : Write the code in this file. and this div is used to describe what do you do (just for tutorial)
            <Converter />
            <Converter2 />
        </div>
    );
}
// `` for copying template literal

class Converter extends Component {
    constructor(props) {
        super(props)
        this.state = { currency : '€' }
    }

    handleChangeCurrency = () => {
        this.setState({
            currency : this.state.currency === '€' ? '$' : '€'
        })
    }

    render() {
        return (
            <div>
                <Display currency={this.state.currency} />
                <CurrencySwitcher
                    currency={this.state.currency}
                    handleChangeCurrency={this.handleChangeCurrency}
                />
            </div>
        )
    }
}

class Converter2 extends Component {
    constructor(props) {
        super(props)
        this.state = { currency : '€' }
        this.handleChangeCurrency = this.handleChangeCurrency.bind(this)
    }

    handleChangeCurrency(event){
        this.setState({
            currency : this.state.currency === '€' ? '$' : '€'
        })
    }

    render() {
        return (
            <div>
                <Display currency={this.state.currency} />
                <CurrencySwitcher
                    currency={this.state.currency}
                    handleChangeCurrency={this.handleChangeCurrency}
                />
            </div>
        )
    }
}


const CurrencySwitcher = props => {
    return (
        <button onClick={props.handleChangeCurrency}>
            Current currency is {props.currency}. Change it!
        </button>
    )
}

const Display = props => {
    return <p>Current currencty is {props.currency}.</p>
}
const TAG_TUTORIAL = "Add name TAG Tutorial here"
let tutorial = "add explanation in here and replace if there's another explanation" 


export default EventJSReact; // TODO Don't forget to change it