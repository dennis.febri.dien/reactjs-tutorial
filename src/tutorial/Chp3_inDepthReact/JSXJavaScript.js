import React from 'react';
import ReactDOM from 'react-dom';

const elements = ['one', 'two', 'three'];
const items = []
for (const [index, value] of elements.entries() ){
    items.push(<li key={index}>{value}</li>)
}

const elements2 = ['satu', 'dua', 'tiga'];



function JSXJavaScript() { // TODO Don't forget to change it
    return (
        <div>
                {headerJavaScript}
                This is tutorial JSX in java
                <textarea defaultValue={TAG_TUTORIAL}></textarea>
                <select defaultValue="x">
                        <option value="y">y</option>
                        <option value="x">x</option>
                        <option value="z">z</option>
                </select>
                <p>&copy; 2017</p>
                <div style={divStyle}>
                    {items}
                </div>
                <div style={divRedStyle}>
                <ul>
                    {elements2.map((value, index) => {
                        return <li key={index}>{value}</li>
                    })}
                </ul>
                </div>
        </div>
    );
}

let divStyle = {
    color : 'blue',
}
let divRedStyle = {
    color : 'red',
}

let name = 'Dennis Febri Dien'
let paragraph = `Hello~, my name is ${name}`

var someStyle = {
    color: 'green',
    backgroundColor :'black',
}

class DataPost {
    constructor() {
        this.title = "This is a title"
        this.name = "this is a name"
    }
} 
// Want to add example of BlogPost spread attributes
// But Currently, it needs learn about React Component
// So i need to hold it off for a while




ReactDOM.render(
    <div id="test1">
        <h1>A Title for Render in JSX</h1>
        <p>{paragraph}</p>
        <p>ReactDOM only can be called if id jsxtest1 from original html
            such as index.html in public
        </p>
        <p className="myClass" style={someStyle}> myClass is a class in html, but they assign it using classname="myClass", not using class="myClass"</p>
        <p>
            Something
            {' '} add text
            {' '} verticals ways
            {/*
                This is how to add comment
            */}
        </p>
    </div>,
    document.getElementById('jsxtest1')
)

console.log("[JSX] you only can use REACTDOM from original html id");
console.log("[JSX] some atttribute change its name such as onchange => onChange (they are using camelCase as standard)")
console.log("[JSX] due to the fact JSX is javascript, class is a reserved word, so they change into this")
console.log("[JSX] For some reason, React still imperative for declaring variable. so you need to declare var somestyle before added it somewhere in below it")
/*
Instead
<div>
<BlogPost title={data.title} date={data.date} />
</div>

<div>
<BlogPost {...data} />
</div>

you can


*/

// `` for copying template literal

const TAG_TUTORIAL = "[JSX]"
let tutorial = "Here's how we define h1tag contain string";

const headerID = 'h1-jsx'
const headerJavaScript = <h1 id={headerID} style={divStyle}>Tutorial JSX</h1>


//utils.explanation(TAG_TUTORIAL,tutorial,"OUTPUT HERE")
//utils.newSubTutorial("Part x: TITLE HERE")

export default JSXJavaScript; // TODO Don't forget to change it