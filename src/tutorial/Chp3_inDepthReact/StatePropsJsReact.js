import React, {Component} from 'react';

function StatePropsJSReact() { // TODO Don't forget to change it
    return (
        <div>
            This is tutorial for React State
            <BlogPostExcerpt />
        </div>
    );
}

// `` for copying template literal

const TAG_TUTORIAL = "[State] "
let tutorial = "This tutorial is about how to use state in React"

console.log(TAG_TUTORIAL,tutorial)

class Person extends Component {
    constructor(name,age) {
        this.name = name
        this.age = age
    }
    render() {
        return (
            <div>
                <p>{this.name}</p>
                <p>{this.age}</p>
            </div>
        )
    }
}


class BlogPostExcerpt extends Component {
    constructor(props) {
        super(props)
        this.state = { 
            clicked: 0,
            elements2: ['satu1', 'dua2', 'tiga3']
        }
    }
    
    addClick = event => {
        this.setState({clicked: (this.state.clicked + 1)})
    }
    render() {
        return (
            <div>
                <h1>Title</h1>
                <p>Description</p>
                <p>Clicked: {this.state.clicked}</p>
                <button onClick={this.addClick}>
                    add clicked
                </button>
                <Converter />
                <ListPerson elements2={this.state.elements2} />
            </div>
        )
    }
}
const elements2 = ['satu', 'dua', 'tiga'];



class ListPerson extends Component {
    constructor(props) {
        super(props)
        this.elements2 = props.elements2
    }

    render() {
        return (
            <ul>
            {this.elements2.map((value, index) => {
                return <li key={index}>{value}</li>
            })}
            </ul>
        )
    }

}

class Converter extends Component {
    constructor(props) {
        super(props)
        this.state = { currency : '€' }
    }

    handleChangeCurrency = event => {
        this.setState({
            currency : this.state.currency === '€' ? '$' : '€'
        })
    }

    render() {
        return (
            <div>
                <Display currency={this.state.currency} />
                <CurrencySwitcher
                    currency={this.state.currency}
                    handleChangeCurrency={this.handleChangeCurrency}
                />
            </div>
        )
    }
}

const CurrencySwitcher = props => {
    return (
        <button onClick={props.handleChangeCurrency}>
            Current currency is {props.currency}. Change it!
        </button>
    )
}

const Display = props => {
    return <p>Current currencty is {props.currency}.</p>
}
    



export default StatePropsJSReact; // TODO Don't forget to change it