import React from 'react';

function FormJSReact() { // TODO Don't forget to change it
    return (
        <div>
            TODO : Write the code in this file. and this div is used to describe what do you do (just for tutorial)
            <Form />
            <FileInput />
        </div>
    );
}

class Form extends React.Component {
    constructor(props) {
        super(props)
        this.state = { value: '' }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    
    handleChange(event) {
        this.setState({
            value: event.target.value
        })
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                Username:
                <input
                    type="text"
                    value={this.state.username}
                    onChange={this.handleChange}
                />
                <input type="submit" value="Submit" />
            </form>
        )
    }
}


class FileInput extends React.Component {
    constructor(props) {
        super(props)
        this.curriculum = React.createRef()
        this.handleSubmit = this.handleSubmit.bind(this)
    }
 
    handleSubmit(event) {
        alert(this.curriculum.current.files[0].name)
        event.preventDefault()
    }
 
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
            <input type="file" ref={this.curriculum} />
            <input type="submit" value="Submit" />
            </form>
        )
    }
}
// `` for copying template literal

export default FormJSReact; // TODO Don't forget to change it