import React from 'react';
import PropTypes from 'prop-types';

function PropTypesJSReact() { // TODO Don't forget to change it
    return (
        <div>
            This is tutorial for PropTypes
            <BlogPostExcerpt />
        </div>
    );
}    

class BlogPostExcerpt extends React.Component {
    render() {
        return (
            <div>
                <h1>{this.props.title}</h1>
                <p>{this.props.npm}</p>
            </div>
        )
    }
}

BlogPostExcerpt.propTypes = {
    title: PropTypes.string.isRequired,
    npm: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        
}

BlogPostExcerpt.defaultProps = {
    title: 'Example use of proptypes String ',
    npm: "1606838193a"
}
    
    
// `` for copying template literal

const TAG_TUTORIAL = "Add name TAG Tutorial here"
let tutorial = "add explanation in here and replace if there's another explanation" 


export default PropTypesJSReact; // TODO Don't forget to change it